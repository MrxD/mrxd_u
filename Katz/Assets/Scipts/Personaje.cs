﻿using UnityEngine;
using System.Collections;

public class Personaje : MonoBehaviour {
    public bool enSuelo = true, impulso = false, corriendo = false;
    private bool contactoBomba = false, puedeSaltar = false, contactoLanzas = false, hurt = false, powerItem = false;
    public Transform comprobadorSuelo, zonaImpulso;
    public float comprobadorSueloRadio = 0.03f, fuerzaSalto = 10.3f, fuerzaImpulso = 14, velocidad = 7, radioZonaImpulso = 0.05f;
    public LayerMask mascaraSuelo, resorte, lanza;
    private Animator animator;
    private Collider2D colliderZonaImpulso;
    private float tiempoPower = 4f;
  


    void Awake() { //se ejecuta a la hora de incializar el objeto, solo una vez
        animator = GetComponent<Animator>();
        colliderZonaImpulso = this.zonaImpulso.GetComponent<CircleCollider2D>(); //obtengo collider de la zona de impulso del personaje
    }

	// Use this for initialization
	void Start () {
        
        
	}

    void FixedUpdate(){
        enSuelo = Physics2D.OverlapCircle(comprobadorSuelo.position, comprobadorSueloRadio, mascaraSuelo); //chequeo en todo momento si el personaje esta en una plataforma con mascara suelo
        impulso = Physics2D.OverlapCircle(zonaImpulso.position, radioZonaImpulso, resorte);
        contactoLanzas = Physics2D.OverlapPoint(zonaImpulso.position, lanza);
        animator.SetBool("isGrounded", enSuelo); //cada vez que el personaje este tocando el suelo o no el animator tambien lo sabra
        animator.SetFloat("velX", GetComponent<Rigidbody2D>().velocity.x);
        animator.SetFloat("velY", GetComponent<Rigidbody2D>().velocity.y);
        if (puedeSaltar){ 
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, fuerzaSalto);
            puedeSaltar = false;
        }
        if (corriendo && !powerItem) //velocidad normal cuando el personaje no obtiene el item PowerUp
            GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, GetComponent<Rigidbody2D>().velocity.y);
        else if (corriendo && powerItem) {  //si se obtiene un powerup se incrementa la velocidad del personaje durante 4 segundos
            GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad + 2, GetComponent<Rigidbody2D>().velocity.y);
            tiempoPower -= Time.deltaTime;
            if (tiempoPower <= 0){
                tiempoPower = 4f; //reseteo el timer
                powerItem = false; //cierro la condicion
            }
        }
        if (contactoBomba && !powerItem){  //si choque contra una bomba siendo invensible la bomba explota pero el personaje no muere
            animator.SetTrigger("death");
            NotificationCenter.DefaultCenter().PostNotification(this, "PersonajeHaMuerto");
            if (!enSuelo) //si muere en el aire, aplico una velocidad de descenso para mostrar mejor la animacion de muerte
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, -2.5f);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0); //la toque estando en el suelo, no aplico velocidad de descenso
            corriendo = false; //se frena la camara
        }
        if (contactoLanzas){ //si hay choque con lanzas por mas que tenga un PowerUp, el personaje muere
            animator.SetTrigger("death");
            NotificationCenter.DefaultCenter().PostNotification(this, "PersonajeHaMuerto");
            if (!enSuelo) //si muere en el aire, aplico una velocidad de descenso para mostrar mejor la animacion de muerte
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, -2.5f);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0); //la toque estando en el suelo, no aplico velocidad de descenso
            corriendo = false; //se frena la camara
        }

        if (impulso){ //si colisione con un resorte cayendo hacia el suelo impulso al personaje
            if (GetComponent<Rigidbody2D>().velocity.y < 0)
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, fuerzaImpulso);
        }
        if (hurt && !contactoBomba && !contactoLanzas && !powerItem){ //si el personaje es tocado por un enemigo activo la animacion que fue tocado
            animator.SetTrigger("hurt");
            hurt = false;
        }
        if (GetComponent<Rigidbody2D>().velocity.y > 0) 
            colliderZonaImpulso.enabled = false;
        else
            colliderZonaImpulso.enabled = true;
       
    }
	
	// Update is called once per frame
    void Update(){       
        if (Input.GetMouseButtonDown(0)){
            if (corriendo){
                if (enSuelo)
                    puedeSaltar = true;
                }
                else{
                    corriendo = true;
                    NotificationCenter.DefaultCenter().PostNotification(this, "PersonajeEmpiezaACorrer"); //notifico para que empieze el efecto scroll y se comiencen a generar las plataformas
                }
            }
    }

    void OnTriggerEnter2D(Collider2D other)
    { //metodo que se ejecuta cuando colisiono con un objeto cuyo collider esta seteado como trigger (bomba, moneda, power, resorte)
        if (other.gameObject.tag == "BombaTierra" || other.gameObject.tag == "BombaAire") //si colisione con una bomba de tierra o de aire el personaje morira
            contactoBomba = true;
        else if (other.gameObject.tag == "Enemigo")
            hurt = true;
        else if (other.gameObject.tag == "ItemPowerUp"){ //si obento un item de tipo power up pongo bandera en true para ejecutar animacion
            animator.SetTrigger("power");
            powerItem = true;
        }
    }

    void OnTriggerExit2D(Collider2D other) { //cuando salgo del collider de la bomba dejo contactoBomba en false
        if (other.gameObject.tag == "BombaTierra" || other.gameObject.tag == "BombaAire")
            contactoBomba = false;
        else if (other.gameObject.tag == "Enemigo")
            hurt = false;
    }

    void OnTriggerStay2D(Collider2D other){
       
    }

    void OnCollisionEnter2D(Collision2D other)
    {
       
    }

  
   

   
}




