﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstaculos : MonoBehaviour
{
    public Obstaculo[] obstaculos;

    // Use this for initialization
    void Start()
    {
        generarMapa();
    }

    void generarObstaculo()
    {
        Obstaculo o = obstaculos[0]; //obtengo el obstaculo plataforma en la posicion del array
        List<Componente> listado = o.getListadoComponentes(); //obtengo el listado de los componentes que forman ese obstaculo
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto; //obtengo el objeto usando las properties
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoDos()
    {
        Obstaculo o = obstaculos[1];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
            {
                if (t.name == "Rampa1") //si es la rampa numero uno, lleva la rotacion seteada en el transform
                    Instantiate(objeto, t.position, t.rotation);
                else
                    Instantiate(objeto, t.position, Quaternion.identity);
            }
        }
    }

    void generarObstaculoTres()
    {
        Obstaculo o = obstaculos[3];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoCuatro()
    {
        Obstaculo o = obstaculos[4];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoCinco()
    {
        Obstaculo o = obstaculos[5];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoSeis()
    {
        Obstaculo o = obstaculos[6];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoSiete()
    {
        Obstaculo o = obstaculos[7];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoOcho()
    {
        Obstaculo o = obstaculos[8];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoNueve()
    {
        Obstaculo o = obstaculos[9];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                if (t.name == "Rampa1")
                    Instantiate(objeto, t.position, t.rotation);
                else
                    Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarObstaculoDiez()
    {
        Obstaculo o = obstaculos[10];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
                if (t.name == "Rampa1")
                    Instantiate(objeto, t.position, t.rotation);
                else
                    Instantiate(objeto, t.position, Quaternion.identity);
        }
    }

    void generarPlataforma()
    {
        Obstaculo o = obstaculos[2];
        List<Componente> listado = o.getListadoComponentes();
        foreach (Componente c in listado)
        {
            GameObject objeto = c.objeto;
            List<Transform> posiciones = c.posiciones;
            foreach (Transform t in posiciones)
            {
                if (t.name == "Rampa1")
                    Instantiate(objeto, t.position, t.rotation);
                else if (t.name == "Rampa4")
                    Instantiate(objeto, t.position, t.rotation);
                else
                    Instantiate(objeto, t.position, Quaternion.identity);
            }
        }
    }

    void generarMapa()
    { //este metodo hara que se generen distintos obstaculos a medida que el personaje avanza
        /*generarObstaculo();
        generarObstaculoDos();
        generarObstaculoTres();
        generarObstaculoCuatro();
        generarObstaculoCinco();
        generarObstaculoSeis();
        generarObstaculoSiete();
        generarObstaculoOcho();
        generarObstaculoNueve();
        generarObstaculoDiez();
        generarPlataforma();*/
        generarObstaculoS();
        Invoke("generarMapa", Random.Range(38f,41f)); //la generacion de los obstaculos se generara en un rango de 12 a 15 segundos
        // Invoke("generarMapa", Random.Range(3.5f, 5f)); //la generacion de los obstaculos se generara en un rango de 12 a 15 segundos
    }

    void generarObstaculoS(){
        for (int i = 0; i < obstaculos.Length; i++){ //recorro todo el array de obstaculo e instancio los mismos
            Obstaculo o = obstaculos[i]; 
            List<Componente> listado = o.getListadoComponentes();
            foreach (Componente c in listado){
                GameObject objeto = c.objeto;
                List<Transform> posiciones = c.posiciones;
                foreach (Transform t in posiciones){
                    if (t.name == "Rampa1")
                        Instantiate(objeto, t.position, t.rotation);
                    else if (t.name == "Rampa4")
                        Instantiate(objeto, t.position, t.rotation);
                    else
                        Instantiate(objeto, t.position, Quaternion.identity);
                }
            }
        }
    }
}
