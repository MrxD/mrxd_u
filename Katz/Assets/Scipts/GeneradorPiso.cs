﻿using UnityEngine;
using System.Collections;

public class GeneradorPiso : MonoBehaviour{

    public GameObject piso;
    public float tiempoMin = 1f;
    public float tiempoMax = 1f;
    public Vector3 separacionGround;
   // public float size = 10.16923f;
    


    // Use this for initialization
    void Start()
    {
        //Vector3 pos = transform.position;
        separacionGround = new Vector3(13.19f, 0, 0);
        generarBloque();
        
    }

    void generarBloque() {
        //Random.Range(0, bloques.Length 
       Instantiate(piso, transform.position, Quaternion.identity); //instancio un bloque de forma randomica segun hayan almacenados en el arreglo
      // transform.position += dir * size; 
       transform.position += separacionGround;
       Invoke("generarBloque", 1.4f);
    }

    
}