﻿using UnityEngine;
using System.Collections;

public class Objeto : MonoBehaviour {
    private Animator animator;
    private bool sinSalto, empiezaSalto;

    void Awake() { //se ejecuta a la hora de incializar el objeto, solo una vez 
        animator = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start(){
      //  NotificationCenter.DefaultCenter().AddObserver(this, "SaltoProhibido");
       // NotificationCenter.DefaultCenter().AddObserver(this, "EmpiezaSaltar");
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player"){ //si colisiono con el personaje
            if (this.gameObject.tag == "ItemPowerUp")   
                Destroy(this.gameObject, 0.10f);
            else if (this.gameObject.tag == "ItemMoneda"){
                animator.SetTrigger("collected");
                Destroy(this.gameObject, 0.6f);
            }
            else if (this.gameObject.tag == "Manzana"){
                animator.SetTrigger("collected");
                Destroy(this.gameObject, 0.60f);
            }
            else if (this.gameObject.tag == "ItemVida")    
                Destroy(this.gameObject, 0.10f);
            else if (this.gameObject.tag == "BombaTierra")
            {
                animator.SetTrigger("boom");
                Destroy(this.gameObject, 0.65f);
            }
            else if (this.gameObject.tag == "BombaAire")
            {
                animator.SetTrigger("airBoom");
                Destroy(this.gameObject, 0.55f);
            }
        }
        else if (collider.tag == "ZonaImpulso"){ //si el resorte colisiona con el collider perteneciente a la impulso del personaje, el resorte fue pisado
            if (this.gameObject.tag == "Resorte"){
                animator.SetTrigger("pisado");
            }
        }
    }

    void Pisado() { 
        
    }

    
}
