﻿using UnityEngine;
using System.Collections;

public class GeneradorNivel1 : MonoBehaviour {
    public GameObject[] enemigos;
    public float tiempoMin = 1.25f;
    public float tiempoMax = 2.75f;
    
	// Use this for initialization
	void Start () {
        NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeEmpiezaACorrer");
	}

    void PersonajeEmpiezaACorrer() {
        GenerarEnemigos();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void GenerarEnemigos() {
        Instantiate(enemigos[Random.Range(0, enemigos.Length)], transform.position, Quaternion.identity);
        Invoke("GenerarEnemigos", Random.Range(tiempoMin, tiempoMax)); //se llama de forma recursiva a este metodo en un rango establecido en las variables tiempoMin tiempoMax
    }
}
