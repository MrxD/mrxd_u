﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {

    public float velocidad = 0f;
    private bool empezo;
    private float tiempoInicio = 0f;
    

	// Use this for initialization
	void Start () {
        NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeEmpiezaACorrer");
        NotificationCenter.DefaultCenter().AddObserver(this, "PersonajeHaMuerto");
	}

    void PersonajeEmpiezaACorrer() {
        empezo = true;
    }

    void PersonajeHaMuerto(){ //se detiene el scroll
        empezo = false;
       
    }

    void UpVelocidadScroll() { 
    
    }


	// Update is called once per frame
    void Update()
    {
        if (empezo)
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(((Time.time - tiempoInicio)) * velocidad % 1, 0); //se le hace el modulo con 1 para quedarnos con la parte decimal y asi evitar fallos en Android    
    }
}
