﻿using UnityEngine;
using System.Collections;

public class SeguirPersonaje : MonoBehaviour {

    public Transform personaje;
    public float espacio = 10.15f;
    public float espacioY = 8f;

	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(personaje.position.x + espacio, transform.position.y, transform.position.z);
	  
	}
}
