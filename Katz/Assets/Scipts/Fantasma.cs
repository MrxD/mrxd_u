﻿using UnityEngine;
using System.Collections;

public class Fantasma : MonoBehaviour {
    public float velocidad;
    public bool mirarDer = false;
    public Transform check1, check2;
    


	// Use this for initialization
	void Start () {
      
	}

    void FixedUpdate(){
        if (mirarDer) //si el fantasma esta mirando hacia la derecha me muevo hacia la derecha
            GetComponent<Rigidbody2D>().transform.Translate(Vector2.right * velocidad* Time.deltaTime); 
        else
            GetComponent<Rigidbody2D>().transform.Translate(-Vector2.right * velocidad * Time.deltaTime);  //sino hacia la izquierda
        if (GetComponent<Rigidbody2D>().transform.position.x >= check2.transform.position.x && mirarDer) //en cada movimiento chequeo la posicion, si es mayor igual a 4 y estoy mirando hacia la derecha, roto para la izquierda
            GirarPersonaje();
        if (GetComponent<Rigidbody2D>().transform.position.x <= check1.transform.position.x && !mirarDer) //si llego a la posicion -1 y estoy mirando hacia la izquierda, roto para la derecha
            GirarPersonaje();
    }

   
	
	// Update is called once per frame
	void Update () { 
        
	}
    
    void GirarPersonaje() {
        mirarDer = !mirarDer; //qued mirando hacia el sentido contrario
        Vector3 escalada = transform.localScale; 
        escalada.x *= -1; //cambio la escalada a -1
        transform.localScale = escalada; //seteo la escalada al fantasma
    }
}
