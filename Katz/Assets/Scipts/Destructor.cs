﻿using UnityEngine;
using System.Collections;

public class Destructor : MonoBehaviour {

    void OnTriggerEnter2D (Collider2D other) { //se destruyen todos los objetos del mapa a medida que el juego avanza
        Destroy(other.gameObject);
    }
}
